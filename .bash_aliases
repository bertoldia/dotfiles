alias reload='source ~/.bashrc'

# Core
alias ll='exa --group-directories-first -lbg'
alias la='exa --group-directories-first -bga'
alias lal='exa --group-directories-first -lbga'
alias q='lcd ../'
alias cd='lcd'
alias mkdir='mkdir -p'
alias df='df -h'
alias du='du -h'
alias locate='locate -e'
alias ffind='find . -iname'
alias archive='file-roller -d'
alias extract='file-roller -h'
alias locate='locate -e'
alias tree='exa --group-directories-first -T'

# Workpaces
alias be='cd ~/workspace/kira/backend/go'
alias re='cd ~/workspace/kira/review/backend/go'

# Typos
alias gti='git'
alias docekr='docker'
alias car='cat'
alias maek='make'

alias gr='git revise'

# Images manipulation
alias momify='_momify'
alias dropboxify='_dropboxify'

alias maketex='rubber --force --inplace --pdf *.tex'
alias cleantex='rm *.aux *.dvi *.out *.ps *.log *.pdf'

# vim
alias nvimrc='avim ~/.config/nvim/init.vim ~/.config/nvim/lua/*.lua'
alias vim-cleanup='rm $HOME/.local/share/nvim/swap/*'

# rc edit shortcuts
alias bashrc='nvim ~/.bashrc'
alias aliasedit='nvim ~/.bash_aliases'
alias gitconfig='nvim ~/.gitconfig'
alias sshconfig='nvim ~/.ssh/config'
alias alacrittyrc='nvim ~/.config/alacritty.yml'
alias kakrc='kak .config/kak/kakrc'

# manjaro
if [ -f /usr/bin/yay ]; then
    alias search='yay -Ss'
    alias install='yay -S'
    alias update='yay -Syyu'
    alias updatea='yay -Syua --noconfirm'
    alias uninstall='yay -Rsc'
    alias orphaned='yay -Qdt'
    alias not_installed='_search_not_installed'
elif [ -f /usr/bin/apt-get ]; then
    alias install='sudo apt-get install'
    alias uninstall='sudo apt-get remove'
fi

if [ -f /usr/bin/colordiff ]; then
    alias diff='colordiff -cp'
else
    alias diff='diff -cp'
fi

alias cpu='htop -s PERCENT_CPU'
alias mem='htop -s M_RESIDENT'
alias dsp='docker system prune -f && docker volume prune -f --filter "label!=com.docker.compose.persist=True"'
alias dvl='docker volume list'
alias dpa='docker ps -a'
alias dsa='docker stop $(docker ps -aq)'
alias di='docker images | sort'
alias df='duf'

# Go
alias gt='go test -count=1'
alias gtI='go test -count=1 --tags=integration'
alias gtv='go test -v -count=1'
alias gotrace='go test -count=1 -race -gcflags=all=-d=checkptr=0'

# Git + GitHub
alias dof='git --git-dir=$HOME/.dotfiles.git'
alias pr-multigo='gh pr list -l Multiverse -l golang -l C++'
alias pr-db='gh pr list -l Database'
alias pr-list='gh pr list'
alias pr-stat='gh pr status'
alias pr-co='gh pr checkout'
alias pr-approve='gh pr review -a'
alias pr-merge='gh pr merge -d -m'
alias lg='lazygit'
alias gg='git grep --color'

function branch-diff() {
    UPSTREAM="origin/$(git default-branch)"
    git diff --name-only "$UPSTREAM" | fzf --preview "git diff --color -w $UPSTREAM {}"
}


#alias less='less -RF'
#alias cat='bat --pager=less'
alias cat='bat'
#alias sed='sad'

function _build_kira.qna_uberjar() {
    cd $(git rev-parse --show-toplevel)/kira.qna
    lein install && lein with-profile -dev uberjar
}

function lcd() {
    builtin cd "$@"
    ll
}

function _search_not_installed() {
    pacman -Ss $1 | pcregrep -vM 'installed.*\n.*'
}

function commit-go-mod() {
    git add go.mod go.sum
    git ci -m "Update go mod"
}

# ZFZ

function is_in_git_repo() {
    git rev-parse HEAD >/dev/null 2>&1
}

function fzf_down() {
    fzf --border "$@"
}

# Use fzf to select a git branch, sorted by local first and most recent commit,
# limit 30 last branches
function __fzf_git_branch() {
    is_in_git_repo || return

    local branches branch selected
    locals=$(git for-each-ref --sort=-committerdate refs/heads/ --format="%(refname:short)") &&
        remotes=$(git for-each-ref --sort=-committerdate refs/remotes/ --format="%(refname:short)") &&
        branches=$(echo -e "${red}${locals}\n${default}${remotes}")
    branch=$(
        echo "$branches" | fzf_down --ansi \
            --preview-window right:65% -d $((2 + $(wc -l <<<"$branches"))) \
            --preview 'git log --oneline --graph --date=short --color=always --pretty="format:%C(auto)%cd %h%d %s" {} | head -' \
            --bind "ctrl-t:execute(tig {1})" \
            --bind "ctrl-d:execute(git br -D {1})" \
    ) &&
        selected="$(echo "$branch" | tr '\f\r\n' ' ')"
    if [[ $# -gt 0 ]]; then
        echo -n "$selected"
    else
        READLINE_LINE="${READLINE_LINE:0:$READLINE_POINT}$selected${READLINE_LINE:$READLINE_POINT}"
        READLINE_POINT=$((READLINE_POINT + ${#selected}))
    fi
}
bind -x'"\eb": __fzf_git_branch'

function isel() {
    # interactively select the output of the specified command
    selected=$("$@" | sort -u | fzf_down --multi --ansi --preview "bat --color=always {}" | tr '\f\n' ' ')
    READLINE_LINE="${READLINE_LINE:0:$READLINE_POINT}$selected${READLINE_LINE:$READLINE_POINT}"
    READLINE_POINT=$((READLINE_POINT + ${#selected}))
}

alias __fzf_git_files='isel git-files origin/$(git default-branch)'
bind -x '"\ep":"__fzf_git_files"'

function __fzf_git_changes() {
    is_in_git_repo || return
    selected=$(git changes | fzf_down --tac --multi --ansi --no-sort --preview 'git show --color=always {1}' | awk '{print $1}')
    READLINE_LINE="${READLINE_LINE:0:$READLINE_POINT}$selected${READLINE_LINE:$READLINE_POINT}"
    READLINE_POINT=$((READLINE_POINT + ${#selected}))
}
bind -x '"\eg":"__fzf_git_changes"'

function avim() {
    alacritty -e nvim "$@" &
}

function alix() {
    alacritty -e helix "$@" &
}

review_tool() {
    gh pr list -l Multiverse -l golang -l C++ | fzf --preview 'gh pr view {1}' | grep -o "[0-9]*" | xargs pr_review
    #pr_review `gh pr list -l Multiverse -l golang | fzf --preview 'gh pr view {1}' | grep -o "^[0-9]*"`
}

function pr_select() {
    is_in_git_repo || return

    selected=$(prtool)
    READLINE_LINE="${READLINE_LINE:0:$READLINE_POINT}$selected${READLINE_LINE:$READLINE_POINT}"
    READLINE_POINT=$((READLINE_POINT + ${#selected}))
}
bind -x '"\ea":"pr_select"'
