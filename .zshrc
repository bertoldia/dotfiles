if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
    source /etc/profile.d/vte.sh
fi

# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle :compinstall filename '/home/axel/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=5000
SAVEHIST=5000
#bindkey -v
# End of lines configured by zsh-newuser-install

#https://superuser.com/questions/585003/searching-through-history-with-up-and-down-arrow-in-zsh
K_UP="^[[A"
K_DOWN="^[[B"
K_HOME="^[[H"
K_END="^[[F"
K_DEL="^[[3~"
K_INS="^[[2~"

bindkey $K_HOME beginning-of-line
bindkey $K_END end-of-line
bindkey $K_INS overwrite-mode
bindkey $K_DEL delete-char

autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey $K_UP up-line-or-beginning-search
bindkey $K_DOWN down-line-or-beginning-search

eval "$(oh-my-posh --init --shell zsh --config ~/.oh-my-posh.json)"

[ -f ~/.zsh_aliases ] && source ~/.zsh_aliases

git_completion=/usr/share/git/completion/git-completion.zsh
zstyle ':completion:*:*:git:*' script $git_completion
fpath=(~/.zsh $fpath)
autoload -Uz compinit && compinit

[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh

export FZF_DEFAULT_OPTS="--no-height --no-reverse"
export FZF_CTRL_T_OPTS="--preview 'highlight -O ansi -l {} 2> /dev/null; or cat {} 2> /dev/null; or exa -T {} 2> /dev/null | head -200'"
export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:hidden:wrap --bind '?:toggle-preview'"
export FZF_ALT_C_OPTS="--preview 'exa -T {} | head -200'"

export EDITOR=nvim

export GLBIN=$HOME/workspace/gitlab/bin
export GOPATH=$HOME/.go
export GOBIN=$GOPATH/bin
export PATH=$HOME/.bin:$PATH:$GOBIN:$GLBIN

# gpg stuff for signing git commits...
export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null
