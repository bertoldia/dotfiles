if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
    source /etc/profile.d/vte.sh
fi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ -f ~/.welcome_screen ]] && . ~/.welcome_screen

_set_liveuser_PS1() {
    PS1='[\u@\h \W]\$ '
    if [ "$(whoami)" = "liveuser" ] ; then
        local iso_version="$(grep ^VERSION= /usr/lib/endeavouros-release 2>/dev/null | cut -d '=' -f 2)"
        if [ -n "$iso_version" ] ; then
            local prefix="eos-"
            local iso_info="$prefix$iso_version"
            PS1="[\u@$iso_info \W]\$ "
        fi
    fi
}
_set_liveuser_PS1
unset -f _set_liveuser_PS1

[[ "$(whoami)" = "root" ]] && return

[[ -z "$FUNCNEST" ]] && export FUNCNEST=100          # limits recursive functions, see 'man bash'

## Use the up and down arrow keys for finding a command in history
## (you can write some initial letters of the command first).
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

alias pacdiff=eos-pacdiff

. /usr/share/fzf/completion.bash
. /usr/share/fzf/key-bindings.bash

alias ll='exa --icons --group-directories-first -l'

git_prompt=/usr/share/git/completion/git-prompt.sh
[ -f $git_prompt ] && source $git_prompt

git_completion=/usr/share/git/completion/git-completion.bash
[ -f $git_completion ] && source $git_completion

[ -f ~/.bash_aliases ] && source ~/.bash_aliases
[ -f /etc/bash_completion ] && ! shopt -oq posix && source /etc/bash_completion

eval "$(oh-my-posh --init --shell bash --config ~/.oh-my-posh.json)"

[ -f ~/.oh-my-git/base.sh ] && source ~/.oh-my-git/base.sh
[ -f ~/.oh-my-git/prompt.sh ] && source ~/.oh-my-git/prompt.sh

[ -f /usr/share/fzf/completion.bash ] && source /usr/share/fzf/completion.bash
[ -f /usr/share/fzf/key-bindings.bash ] && source /usr/share/fzf/key-bindings.bash

export FZF_DEFAULT_OPTS="--no-height --no-reverse"
export FZF_CTRL_T_OPTS="--exact --preview 'highlight -O ansi -l {} 2> /dev/null; or cat {} 2> /dev/null; or exa -T {} 2> /dev/null | head -200'"
export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:hidden:wrap --bind '?:toggle-preview'"
export FZF_ALT_C_OPTS="--preview 'exa -T {} | head -200'"

export INPUTRC=~/.inputrc
export EDITOR=nvim
#export PAGER=bat

export GLBIN=/home/axel/workspace/gitlab/bin
export GOPATH=$HOME/.go
export GOBIN=$GOPATH/bin
export PATH=$HOME/.bin:$PATH:$GOBIN:$GLBIN

# gpg stuff for signing git commits...
export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null
