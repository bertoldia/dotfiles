vim.opt.background = "dark"
vim.g.gruvbox_contrast_dark = true
vim.g.gruvbox_invert_signs = 1
vim.g.gruvbox_color_column = "neutral_blue"
vim.g.gruvbox_vert_split = "bright_yellow"
vim.g.gruvbox_italicize_strings = 1
vim.g.gruvbox_italicize_comments = 1
vim.g.gruvbox_invert_tabline = 1
vim.g.gruvbox_improved_strings = 1
vim.g.gruvbox_improved_warnings = 1

return {
  -- add gruvbox
  { "ellisonleao/gruvbox.nvim" },

  -- Configure LazyVim to load gruvbox
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "gruvbox",
    },
  },
}
