return {
  "lewis6991/gitsigns.nvim",
  opts = {
    current_line_blame = true,
    numhl = true,
    word_diff = false,
  },
  keys = {
    { "]c", '<cmd>lua require"gitsigns".next_hunk()<cr>', desc = "Go to next diffhunk" },
    { "[c", '<cmd>lua require"gitsigns".prev_hunk()<cr>', desc = "Go to previous diffhunk" },
  },
}
