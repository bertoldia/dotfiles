return {
  "neovim/nvim-lspconfig",
  opts = {
    servers = {
      gopls = {
        cmd = { "gopls", "-remote=auto" },
        settings = {
          gopls = {
            analyses = {
              unusedparams = true,
              unreachable = true,
              fieldalignment = true,
              shadow = true,
            },
            staticcheck = true,
            codelenses = { gc_details = true },
            gofumpt = true,
            usePlaceholders = true,
          },
        },
      },
    },
  },
}
