return {
  "nvim-telescope/telescope.nvim",
  keys = {
    { "<leader>rg", require("telescope.builtin").grep_string, "Grep for the string under the cursor" },
    {
      "<leader>.",
      ":lua require('telescope.builtin').find_files({hidden=true, cwd=vim.fn.expand('%:p:h') })<CR>",
      "Search for files in the current file's folder",
    },
  },
}

-- {
--   "<leader>fp",
--   fugction() require("telescope.builtin").find_files({ cwd = require("lazy.core.config").options.root }) end,
--   desc = "Find Plugin File",
-- },
