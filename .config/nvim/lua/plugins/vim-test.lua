vim.keymap.set("n", "<leader>tn", ":TestNearest<CR>", { silent = true })
vim.keymap.set("n", "<leader>tf", ":TestFile<CR>", { silent = true })
vim.keymap.set("n", "<leader>ts", ":TestSuite<CR>", { silent = true })
vim.keymap.set("n", "<leader>tl", ":TestLast<CR>", { silent = true })
vim.keymap.set("n", "<leader>tv", ":TestVisit<CR>", { silent = true })

vim.g["test#strategy"] = "neovim"
vim.g["test#go#gotest#options"] = "-count=1"

return {
  "vim-test/vim-test",
}
